BINARY_BASE_NAME=WSB
BOARD_NAME=WSBFL

COMMON_LIB_SRC = userCan.c debug.c state_machine.c CRC_CALC.c FreeRTOS_CLI.c freertos_openocd_hack.c watchdog.c generalErrorHandler.c
COMMON_F0_LIB_SRC = userCanF0.c

# one of NUCLEO_F7, F7, NUCLEO_F0, F0
BOARD_TYPE=F0

CUBE_F0_MAKEFILE_PATH= Cube-F0-Src/2018_WSB/
CUBE_NUCLEO_F0_MAKEFILE_PATH = Cube-Nucleo-Src/WSB/


include common-all/tail.mk
